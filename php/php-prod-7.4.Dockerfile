FROM php:7.4-cli-alpine

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions &&\
    mv /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini &&\
    install-php-extensions @composer pcntl
